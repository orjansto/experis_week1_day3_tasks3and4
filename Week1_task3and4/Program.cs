﻿using System;

namespace Week1_task3and4
{
    class Program
    {
        static void okToBeSquare() /*Program That lets user draw Square*/
        {
            int size;
            bool success;
            string cont;
            Console.WriteLine("This Program will draw a square!");

            do
            {
                
                Console.WriteLine("How many rows should the square span?");
                do
                {
                    success = Int32.TryParse(Console.ReadLine().Trim(), out size);
                    if (!success || size > 100)
                    {
                        Console.WriteLine("Plese enter a nuber less than 100");
                    }
                } while (!success || size > 100);


                string[] rectArray = createSquareInStringArray(size, size);
                drawRectangleFromStringArray(rectArray);

                Console.WriteLine("Whould you like to draw another square? y/n");

                do
                {
                    cont = Console.ReadLine().Trim();
                    if (!(cont == "y" || cont == "n"))
                    {
                        Console.WriteLine("Please enter 'y' or 'n'");
                    }
                } while (!(cont == "y" || cont == "n"));

            } while (cont == "y");
            
        }
        static void recTangled() /*Program that lets user draw a rectangle*/
        {
            int row;
            int col;
            bool success;
            string cont;
            string[] rectArray = null;
            Console.WriteLine("This Program will draw a rectangled rectangle of the size AxB!");
            Console.WriteLine("The rectangle can be filled with additional rectangles if wanted!");

            do
            {
                Console.WriteLine("How many elements should A (cols) be?");
                do
                {
                    success = Int32.TryParse(Console.ReadLine().Trim(), out col);
                    if (!success || col > 100 || col < 7)
                    {
                        Console.WriteLine("Plese enter a number less than 100 or larger than 6");
                    }
                } while (!success || col > 100 || col < 6);
                Console.WriteLine("How many elements should B (rows) be?");
                do
                {
                    success = Int32.TryParse(Console.ReadLine().Trim(), out row);
                    if (!success || row > 100 || row < 6)
                    {
                        Console.WriteLine("Plese enter a nuber less than 100 or larger than 6");
                    }
                } while (!success || row > 100 || row < 6);

                rectArray = createSquareInStringArray(col, row, rectArray);

                if (rectArray != null)/*Check for illegal arrays (null)*/
                {
                    //Only draw initial innerborder
                    rectArray = createSquareInStringArray(rectArray[0].Length - 4, rectArray.Length - 4, rectArray);
                    drawRectangleFromStringArray(rectArray);
                    Console.WriteLine("Whould you like to draw another rectangle inside your previous one? y/n");
                }
                else
                {
                    Console.WriteLine("Illegal input!!");
                    Console.WriteLine("Do you want to make a new Rectangle? y/n");
                }
                /*Let user re run the program*/
                do
                {
                    cont = Console.ReadLine().Trim();
                    if (!(cont == "y" || cont == "n"))
                    {
                        Console.WriteLine("Please enter 'y' or 'n'");
                    }
                } while (!(cont == "y" || cont == "n"));

            } while (cont == "y");

        }
        static void drawRectangleFromStringArray(string[] rectRows)
        {
            for (int i = 0; i < rectRows.Length; i++)
            {
                 Console.WriteLine(rectRows[i]);
            }
        }
        static string[] createSquareInStringArray(int col, int row, string[] rectRows = null)
        {
            bool drawInsideExist = false;

            /*Checking legal input*/
            if (col < 2 || row < 2)
            {
                Console.WriteLine("ERROR: Rectangle input is incorrect, add values greater than 1");
                return null;
            }

            char[] rows = new char[col];

            /*creating string array if not present*/
            if (rectRows == null)
            {
                rectRows = new string[row];
                rectRows[0] = new string(rows);
            }
            else if (rectRows.Length < row || rectRows[0].Length < col)
            {
                Console.WriteLine("Input rectangle is too small");
                return null;
            }
            else if (true)
            {
                drawInsideExist = true;
                rows = new char[rectRows[0].Length];
            }

            if (!drawInsideExist)
            {
                for (int i = 0; i < rectRows.Length; i++)
                {
                    for (int j = 0; j < (rectRows[0].Length); j++)
                    {
                        if ((j == 0) || (j == (rectRows[0].Length - 1)) || (i == 0) || (i == (rectRows.Length - 1)))
                        {
                            rows[j] = '#';
                        }
                        else
                        {
                            rows[j] = ' ';
                        }
                    }
                    rectRows[i] = new string(rows);
                }
                return rectRows;
            }
            else
            {
                int newBorderCol = (rectRows[0].Length - col) / 2;
                int newBorderRow = (rectRows.Length - row) / 2;
                for (int i = 0; i < rectRows.Length; i++)
                {
                    rows = rectRows[i].ToCharArray();
                    for (int j = 0; j < (rectRows[0].Length); j++)
                    {
                        if ((j == newBorderCol || (j == (rectRows[0].Length - newBorderCol -1)) || (i == newBorderRow) || (i == (rectRows.Length - newBorderRow - 1)))&&!(j<newBorderCol ||i<newBorderRow||j> (rectRows[0].Length) - newBorderCol-1||i> (rectRows.Length - newBorderRow-1)))
                        {
                            rows[j] = '#';
                        }
                    }
                    rectRows[i] = new string(rows);
                }
                return rectRows;
            }
        }
        static void task3() /*Program that runs through content of task3*/
        {
            Console.WriteLine("----Task 3----");
            okToBeSquare();
        }
        static void task4() /*Program that runs through content of task4*/
        {
            Console.WriteLine("----Task 4----");
            recTangled();
        }
        static void Main(string[] args)
        {
            task3();
            task4();
            
        }
    }
}
